const smileys = require('./smileys.js');
const allergensmileys = require('./allergensmileys.js');
const https = require('https');
const config = require('./config.js');

const isBlockedMeal = new RegExp(config.blackMealList.join('|'), 'i');
const isBlockedComponent = new RegExp(config.blackComponentList.join('|'), 'i');

let cache = {};

const addDots = (str) => {
    if (str in config.myDots) {
        return config.myDots[str];
    } else {
        return '◾️ ';
    }
};

const addSmiley = (mealName) => {
    let returnText = '';
    smileys.forEach(smiley => {
        const reg = new RegExp(`${smiley.name}`, 'i');
        if (reg.test(mealName)) {
            returnText += ` ${smiley.emoji}`;
        }
    });
    return returnText;
};

const addAllergenSmiley = (notices) => {
    let returnText = '';
    let empty = true;
    notices.forEach(notice => {
        if (allergensmileys[notice]) {
            if (empty) {
                returnText += ' ('
                empty = false
            }
            returnText += allergensmileys[notice];
        }
    });

    if(!empty) {
        returnText += ')'
    }
    return returnText;
};

const counterToString = (counters, makeBold, ret) => {
    if (!makeBold) {
        makeBold = (x) => x;
    }
    if (ret === undefined) {
        ret = [];
    }
    counters.forEach(counter => {
        returnText = '';
        counter.meals.forEach(meal => {
            if (isBlockedMeal.test(meal.name)) { return; }

            returnText += addDots(counter.id);

            if (meal.category) {
                returnText += `${makeBold(meal.category)} : `;
            }

            returnText += `${meal.name}`;

            returnText += addSmiley(meal.name);

            if (meal.prices) {
                returnText += `  (${meal.prices.s}€)`;
            }

            returnText += '\n';

            meal.components.forEach(component => {
                if (!isBlockedComponent.test(component.name)) {
                    returnText += `    ${config.subDot}${component.name}`;
                    returnText += addSmiley(component.name);
                    returnText += addAllergenSmiley(component.notices);
                    returnText += '\n';
                }
            });
        });
        ret.push(returnText);
    });
    return ret;
};

const download = url => new Promise( (succ, err) => {
    console.log("downloading from api...");
    https.get(url, res => {
        let data = "";
        res.setEncoding('utf8');
        res.on('data', chunk => { data += chunk; });
        res.on('end', () => succ(JSON.parse(data)));
    }).on('error', err);
});
const generateCache = (mensaKey) => Promise.all(config.urls.map(replaceKey(mensaKey)).map(download));

const replaceKey = key => value => value.replace("KEY", key);
const createHeader = (day, end) => {
    const d = new Date(day.date);
    let weekday = config.weekdays[d.getDay()];
    end.push(`Menü für ${weekday}, den ${d.getDate()}. ${config.months[d.getMonth()]} ${d.getFullYear()}:\n`);
};

const getCache = (name, generator) => {
    if (cache[name] === undefined || Date.now() - cache[name].date > config.CACHE_TIME_TO_LIVE) {
        promise = generator();
        cache[name] = {
            data: promise,
            date: Date.now(),
        };
        return promise;
    } else {
        return cache[name].data;
    }
}

const countSchnitzel = data => {
    let schnitzelCounter = 0;
    for (i=0; i<data.length; i++) {
        let match = data[i].match(config.schnitzelRegExp);
        if (match !== null && match.length > 0) {
            schnitzelCounter += match.length;
        }
    }
    return schnitzelCounter;
}

const generateMenu = (index, mensaKey, makeBold) => getCache("json", () => generateCache(mensaKey)).then(
    jsons => {
        //jsons : list of json objects, one per config.urls
        let end = [];

        jsons.forEach( json => {
            if (json.days === undefined) {
                //throwing inside then will reject the promise
                throw (`API-FAIL: ${json.error}\n${json.description}\n`);
            }
            const day = json.days.filter(day => !day.isPast)[index];
            if (day) {
                if (end.length === 0) {
                    createHeader(day, end);
                }
                counterToString(day.counters, makeBold, end);
            }
        });
        const schnitzel = countSchnitzel(end);
        if (schnitzel > 0) {
            end.push(`${config.schnitzelHeaderText}\n${config.schnitzelNotEmpty(schnitzel)}`);
        } else {
            end.push(`${config.schnitzelHeaderText}\n${config.schnitzelEmpty}`);
        }
        return end;
    },
);

const mensamenu = (index, mensaKey, makeBold) => getCache(index, () => generateMenu(index, mensaKey, makeBold));

//TODO: cache this one too
const schnitzel = (index, mensaKey, makeBold) => mensamenu(index, mensaKey, makeBold).then(data => {
    if (data[0] === undefined) {
        throw ("FAIL: maybe offset too big?");
    }

    data = data.map(x => x.replace(config.schnitzelRegExp, makeBold("Schnitzel")));

    data[0] = data[0].replace("Menü", "Schnitzel");
    return data;
}, err => console.log)

module.exports.mensamenu = mensamenu
module.exports.schnitzel = schnitzel
