const config = {
    myDots : {
        komplett: '🅰️  ',
        vegetarisch: '🅱️  ',
        mensacafe: '☕️  ',
        'mensacafe-abend': '🌇  ',
        freeflow: '🆓  ',
        mensagarten: '🏝  ',
    },
    subDot : '▪︎ ',
    blackMealList : [
        'Salatbuffet',
    ],
    blackComponentList : [
        'Salatso.e',
        'Alternativ Beilage',
    ],
    weekdays : [
        "Sonntag",
        "Montag",
        "Dienstag",
        "Mittwoch",
        "Donnerstag",
        "Freitag",
        "Samstag",
    ],
    months : [
        "Januar",
        "Februar",
        "März",
        "April",
        "Mai",
        "Juni",
        "Juli",
        "August",
        "September",
        "Oktober",
        "November",
        "Dezember",
    ],
    urls : [
        `https://mensaar.de/api/2/KEY/1/de/getMenu/sb`,
        `https://mensaar.de/api/2/KEY/1/de/getMenu/mensagarten`,
    ],
    schnitzelRegExp : /schnitzel/ig,
    schnitzelHeaderText : 'Gibt es Schnitzel in der Mensa?!',
    schnitzelEmpty : 'Nein.',
    schnitzelNotEmpty : (i) => `Ja, und zwar ${i} Mal!`,
    //5 minutes, is in milliseconds
    CACHE_TIME_TO_LIVE : 5 * 60 * 1000,
};

module.exports = config;