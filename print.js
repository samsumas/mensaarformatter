const mensa = require('./lib.js');
require('dotenv').config();

const print = x => {
    //console.log(x);
    console.log(x.join('\n'));
}

const main = async () => {
mensa.schnitzel(0, process.env.KEY, x => `<b>${x}</b>`).then(print)
mensa.schnitzel(1, process.env.KEY, x => `<b>${x}</b>`).then(print)
//mensa.schnitzel(2, process.env.KEY, x => `<b>${x}</b>`).then(print)
//mensa.schnitzel(3, process.env.KEY, x => `<b>${x}</b>`).then(print)
mensa.mensamenu(0, process.env.KEY, x => `<b>${x}</b>`).then(print);
mensa.mensamenu(1, process.env.KEY, x => `<b>${x}</b>`).then(print);
//await mensa.mensamenu(0, process.env.KEY, x => `<b>${x}</b>`).then(print);
//await mensa.mensamenu(1, process.env.KEY, x => `<b>${x}</b>`).then(print);
//await mensa.mensamenu(1, process.env.KEY, x => `<b>${x}</b>`).then(print);
//await mensa.mensamenu(1, process.env.KEY, x => `<b>${x}</b>`).then(print);
}
main();
//setTimeout( () => {
//    for (i=0; i<5; i++) {
//        mensa.mensamenu(0, process.env.KEY, x => `<b>${x}</b>`).then(print);
//    }
//}, 1000);